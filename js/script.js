
var init = function () {
    var isMobile = navigator.userAgent &&
        navigator.userAgent.toLowerCase().indexOf('mobile') >= 0;
    var isSmall = window.innerWidth < 1000;

    var ps = new ParticleSlider({
        ptlGap: isMobile || isSmall ? 3 : 0,
        ptlSize: isMobile || isSmall ? 3 : 1,
        width: 1e9,
        height: 1e9
    });

    var gui = new dat.GUI();
    gui.add(ps, 'ptlGap').min(0).max(5).step(1).onChange(function () {
        ps.init(true);
    });
    gui.add(ps, 'ptlSize').min(1).max(5).step(1).onChange(function () {
        ps.init(true);
    });
    gui.add(ps, 'restless');
    gui.addColor(ps, 'color').onChange(function (value) {
        ps.monochrome = true;
        ps.setColor(value);
        ps.init(true);
    });


    (window.addEventListener
        ? window.addEventListener('click', function () { ps.init(true) }, false)
        : window.onclick = function () { ps.init(true) });
}

var initParticleSlider = function () {
    var psScript = document.createElement('script');
    (psScript.addEventListener
        ? psScript.addEventListener('load', init, false)
        : psScript.onload = init);
    psScript.src = 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/23500/ps-0.9.js';
    psScript.setAttribute('type', 'text/javascript');
    document.body.appendChild(psScript);
}

    (window.addEventListener
        ? window.addEventListener('load', initParticleSlider, false)
        : window.onload = initParticleSlider);

$(document).ready(function () {

    $('.burgerMenu').on('click', function () {
        $('.burgerMenu').toggleClass('click');
        $('.menuList').toggleClass('open');
        $('.menuBtn').toggleClass('active');
    });

    /*$(".navigation ").on("click", function (event) {
        event.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top - 100;
        $('body,html').css('translate-3d', '');

        $('.burgerMenu').removeClass('click');
        $('.menuList').removeClass('open');
        $('.menuBtn').removeClass('active');
    });*/

    if ($(document).width() < 769) {
        $(".navigation").on("click", "a", function (event) {
            event.preventDefault();
            var id = $(this).attr('href'),
                top = $(id).offset().top - 100;
            $('body,html').animate({
                scrollTop: top
            }, 1500);

            $('.burgerMenu').removeClass('click');
            $('.menuList').removeClass('open');
            $('.menuBtn').removeClass('active');
        });
    }

});

if ($(document).width() > 769) {

    function _defineProperty(obj, key, value) {
        if (key in obj) {
            Object.defineProperty(obj, key, {
                value: value,
                enumerable: true,
                configurable: true,
                writable: true
            });
        } else {
            obj[key] = value;
        }
        return obj;
    } class HorizontalScrollPlugin extends Scrollbar.ScrollbarPlugin {

        transformDelta(delta, fromEvent) {
            if (!/wheel/.test(fromEvent.type)) {
                return delta;
            }

            // @see: https://github.com/idiotWu/smooth-scrollbar/issues/181

            const { x, y } = delta;

            return {
                y: 0,
                x: Math.abs(x) > Math.abs(y) ? x : y
                // x: Math.sign(x || y) * Math.sqrt(x*x + y*y),
            };
        }
    } _defineProperty(HorizontalScrollPlugin, "pluginName", 'horizontalScroll');


    Scrollbar.use(HorizontalScrollPlugin, OverscrollPlugin /* you forgot this */);

    // the following is forked from https://codepen.io/supah/pen/vvNmOr

    let option = {
        x: 0,
        speed: 1.5,
        limit: 2,
        time: 0.3
    };

    /*--------------------
        Init Scrollbar
    -------------------*/
    const scrollbar = Scrollbar.init(document.querySelector('#scrollbar'), {
        overscrollEffect: 'bounce',
        alwaysShowTracks: false,
    });

    Scrollbar.detachStyle();

    // scrollbar.scrollLeft = 1024; // setPosition(1024, offset.y);
    // console.log(scrollbar.offset.x); // 1024

    scrollbar.addListener(function (status) {
        // Content Feature Header
        $(".anim").each(function () {
            var $this = $(this);

            if (scrollbar.isVisible(this)) {
                $this.addClass("in-view");
            }
        });
    });

    var contentIdThree = document.querySelector('#three');
    var contentIdFour = document.querySelector('#four');
    var contentIdSeven = document.querySelector('#seven');

    const listener = status => {

        if (scrollbar.isVisible(contentIdThree)) {
            $('.additionalHeader').css('left', (0 + (status.offset.x * 0.009)) + 'vw');
            // $('.photo-1').css('background-position', (0 - (status.offset.x * 0.06)) + 'px');
            // $('.photo-2').css('left', (30 - (status.offset.x * 0.003)) + '%');
            $('.photo-3').css('background-position', (0 - (status.offset.x * 0.08)) + 'px');
        }
        // if (scrollbar.isVisible(contentIdFour)) {
        $('.photo-5').css('background-position-x', (-300 + (status.offset.x * 0.06)) + 'px');
        $('.photo-6').css('background-position-x', (80 - (status.offset.x * 0.03)) + 'px');
        // $('.photo-7').css('background-position-x', (-150 + (status.offset.x * 0.03)) + 'px');
        $('#style').css('top', (120 - (status.offset.x * 0.02)) + '%');
        // }
        // if (scrollbar.isVisible(contentIdSeven)) {
        $('#price').css('top', (250 - (status.offset.x * 0.02)) + '%');
        $('.photo-8').css('background-position', (850 - (status.offset.x * 0.08)) + 'px');

        // console.log(status.offset.x);
        // }
    };

    

    var lis = [].slice.call(document.querySelectorAll('.link'));

    let elementLink = document.querySelectorAll('.link');

    document.onclick = function (e) {
        // if($("a").hasClass('link')) {
        //     console.log('fdfd');
        //     e.preventDefault();
        // }
        if (e.target.nodeName !== 'A') return false;
        // var target = $(e.target);
        // if (target.attr("a[@href^='http']")) return false;
        // e.preventDefault();

        

        // return false;


        var Elem;

        lis.some(function (el, i) {
            return el === e.target && ((Elem = i) || true);
        });

        switch (Elem) {
            case 0:
                scrollbar.scrollIntoView(document.querySelector('.aboutUs'), {
                    offsetLeft: 80,
                });
                break;

            case 1:
                scrollbar.scrollIntoView(document.querySelector('.servicesBlock'), {
                    offsetLeft: 80,
                });
                break;

            case 2:
                scrollbar.scrollIntoView(document.querySelector('.team'), {
                    offsetLeft: 80,
                });
                break;

            case 3:
                scrollbar.scrollIntoView(document.querySelector('.contact'), {
                    offsetLeft: 80,
                });
                break;
        }

        
        $('.burgerMenu').removeClass('click');
        $('.menuList').removeClass('open');
        $('.menuBtn').removeClass('active');

        // return true;
    };


    // scrollbar.scrollIntoView(document.querySelector('#about'), {
    //     offsetLeft: 34,
    //     offsetBottom: 0,
    //     alignToTop: false
    // });

    // scrollbar.scrollLeft = 1270;


    scrollbar.addListener(listener);

    setTimeout(function () {
        $('.preload').fadeOut(1000);
    }, 7000);



}








